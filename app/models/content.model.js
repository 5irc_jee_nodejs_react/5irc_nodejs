var fs = require('fs');
var CONFIG = JSON.parse(process.env.CONFIG);
const utils = require("../utils/utils.js");
var path = require("path");


module.exports = class ContentModel {

    /**
     * @brief Model constructor is basically setting all params
     * @param object containing all data
     */
    constructor(object) {
        if (object !== undefined){
            this.type = object.type;
            this.id = object.id;
            this.title = object.title;
            this.src = object.src;
            this.fileName = object.fileName;
        }
    }

    /**
     * @brief Return the path to the image file
     */
    getData(){
        let result = null;
        if (this.type === "img"){
            result = utils.getDataFilePath(this.fileName);
        }
        return result;
    }

    /**
     * @brief Copy the file from the upload directory to the content directory
     * @param data object containing all info regarding the uploaded file
     */
    setData(data){
        fs.copyFile(data.path, CONFIG.contentDirectory + "/" + this.fileName, (err) => {
            if (err) return err;
        });
        return "success";
    }

    /**
     * @brief Create a new content file
     * @param content object containing all data
     * @param response Object used to return the result of the action
     * @param next The next action in the controller (called if no error occured
     */
    static create (content, response, next){
        content.id = utils.generateUUID();

        if (content.type === "img"){
            content.fileName = content.id+path.extname(content.fileName);
            content.src = "/contents/"+content.id;
        }

        fs.writeFile(CONFIG.contentDirectory+"/"+content.id+".meta.json", JSON.stringify(content), (err) => {
            if (err) {
                response.status(500);
                response.json({"error":err});
            }
            else{
                next();
            }
        });
    }

    /**
     * @brief Read all metadata of a given content
     * @param id content id
     * @param callback Object used to return the result of the action to the controller
     */
    static read (id, callback){
        let fileName = utils.getMetaFilePath(id);

        utils.readFileIfExists(fileName, function (err, data) {
            if (err) {
                callback({"error":err});
            }
            else {
                callback(JSON.parse(data));
            }
        });
    }
}
