let fs = require('fs');
let CONFIG = JSON.parse(process.env.CONFIG);
let path = require("path");

let ContentModel = require("../models/content.model");


module.exports = class ContentController {
    constructor(){}

    /**
     * @brief This function list all content
     * @param response Object (used to return result)
     * @return JSON string with all content
     */
    static list(response){
        let result = {};

        //Read the content of the directory
        fs.readdir(CONFIG.contentDirectory, (err, data) => {
            //If an error occur then return it
            if (err){
                response.status(500);
                response.json(err);
            }
            else{
                //If no error then parse all files found in the directory
                data.forEach(fileName => {
                    if (path.extname(fileName) === ".json") {
                        let content = fs.readFileSync(CONFIG.contentDirectory+"/"+fileName);
                        let jsonContent = JSON.parse(content);
                        result[jsonContent.id] = jsonContent;
                    }
                });
                response.json(result);
            }
        });
    }

    /**
     * @brief This function create a new content
     * @param response Object (used to return result)
     * @param request Object with all data to save
     * @return JSON string with error or a message
     */
    static create(request, response){
        let jsonContent = JSON.parse(request.body.data);
        let contentmodel = new ContentModel(jsonContent);

        if (jsonContent.type === "img"){
            if (request.file === undefined){
                //If the content is IMG and the file is not given in the call then throw an error
                response.status(400);
                return response.json({"error":"Missing image!"});
            }
            else {
                contentmodel.fileName = request.file.originalname;
            }
        }

        ContentModel.create(contentmodel, response, function() {
            if (jsonContent.type === "img"){
                //Try to set data (save the img file)
                let tempError = contentmodel.setData(request.file);

                //Trivial way to manage errors
                if (tempError !== "success") {
                    response.status(500);
                    return response.json(tempError);
                }
            }
            return response.json({"success":"Content saved!"});
        });
    }

    /**
     * @brief This read the data of a content (img file or redirect to the video
     * @param response Object (used to return result)
     * @param request Object with params
     * @return JSON string with all content
     */
    static read(request, response){
        ContentModel.read(request.contentId, (jsonContentModel) => {
            if (jsonContentModel.erreur === undefined) {
                if (request.metaJson) {
                    response.json(jsonContentModel);
                }
                else {
                    //If the content is an img then return the file
                    if (jsonContentModel.type === "img") {
                        const contentmodel = new ContentModel(jsonContentModel);
                        response.sendFile(contentmodel.getData());
                    }
                    //Else redirect to the correct URL
                    else {
                        response.redirect(jsonContentModel.src);
                    }
                }
            }
            else {
                //If an error occur
                response.status(500);
                return response.json(jsonContentModel);
            }
        });
    }
}
