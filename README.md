# NodeJS WS

This NodeJS project provides a restful webservice with all the different actions needed to manage a presentation and related content.

# Setup the project
On linux:

- Clone the repo
- Properly setup the path in config.json
- init npm and install all required package (express & mocha)
- start the app using npm

# Setup Postman
- Import the Postman collection from the json file available in /Postman
- Setup the different variable (URI and Authentication) by right clicking on the collection->edit->Authentication/Variable
- Most of request has example (on top right) providing different request/response

# Routes available
- The entire API is documented using Postman: https://documenter.getpostman.com/view/5942755/RzZFDGYP
- On this page it's possible to display different output by clicking on "example request".

# Authentication
Authentication middleware is based on a JEE module that could be setup in the configuration file

# Configuration file
This application could be setup in the configuration file config.json
- port: The port used by nodeJS app to bind
- contentDirectory: The directory used to save all content and linked data
- presentationDirectory: The directory used to save all presentation and linked data
- uploadDirectory: The directory used by the uploader to save temp files
- enableAuthentication: Boolean to enable/disable authentication
- authenticationUri: URI to to JEE module