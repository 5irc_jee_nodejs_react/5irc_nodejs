const requestAuth = require('request');
var CONFIG = JSON.parse(process.env.CONFIG);

module.exports = class LoginController {
    constructor() {
    }

    /**
     * @brief Manage all authentication process for the whole application
     * @param adminRequired boolean true=>admin privilege required, false=>note required
     */
    static CheckAuthentication(adminRequired){
        return function (requestGlobal, responseGlobal, nextGlobal){
            //If the auth is enabled in conf file
            if (CONFIG.enableAuthentication) {
                //Decode the auth header (base64) to a proper jSON string
                let decodedAuth = LoginController.ConvertBase64ToJson(requestGlobal.headers.authorization);
                console.log(decodedAuth);

                //Setup all options
                var options = {
                    uri: CONFIG.authenticationUri,
                    method: 'POST',
                    json: decodedAuth
                };

                //Request the JEE web service
                requestAuth(options, function (error, response, body) {
                    //If the auth success
                    if (!error && response.statusCode === 200) {
                        console.log(body);
                        //If the auth is ok (correct login and pwd) and yes it's not a boolean........
                        if (body.validAuth === 'true')
                        {
                            //Check the admin privilege if required
                            if (adminRequired && body.role !== "admin"){
                                //If not return forbidden code
                                responseGlobal.status(403);
                                responseGlobal.json({"error":"Authentication failed"});
                            }
                            else {
                                //Auth success
                                return nextGlobal();
                            }
                        }
                        else {
                            //If not return forbidden code
                            responseGlobal.status(403);
                            responseGlobal.json({"error":"Authentication failed"});
                        }
                    }
                    //If it fails, then return error
                    else{
                        responseGlobal.status(401);
                        responseGlobal.json({"error":"Authentication failed"});
                    }
                });
            }
            else {
                //Auth success
                return nextGlobal();
            }
        }
    }

    /**
     * @brief Decode Auth token to JSON
     * @param AuthenticationToken Base64 authentication token
     * @returns JSON string with login and password
     */
    static ConvertBase64ToJson(AuthenticationToken){
        //Remove the "Basic " part
        let tempString = AuthenticationToken.substr(AuthenticationToken.indexOf(' ')+1);
        //Create a buffer useful for the conversion
        let buff = new Buffer(tempString, 'base64');
        //Convert from base64 to a readable string
        tempString = buff.toString('ascii');
        //Split ":"
        let arrayResult = tempString.split(':');
        //Generate the json and return it
        return JSON.parse('{"login":"'+arrayResult[0]+'","pwd":"'+arrayResult[1]+'"}');
    }
}