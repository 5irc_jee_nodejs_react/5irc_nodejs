var fs = require('fs');
var CONFIG = JSON.parse(process.env.CONFIG);
const utils = require("../utils/utils.js");
var path = require("path");


module.exports = class PresentationModel {

    constructor(object) {
        this.id = object.id;
        this.title = object.title;
        this.description = object.description;
        this.slides = object.slides;
    }

    /**
     * @brief This function replace all slides of a given presentation
     * @param response Object (used to return result)
     * @param slides JSON string with all slides info
     * @return JSON string with a message
     */
    setSlides(slides, response){
        //Update the sides attribute
        this.slides = slides;

        //Then write the file with the object
        fs.writeFile(CONFIG.presentationDirectory+"/"+this.id+".pres.json", JSON.stringify(this), (err) => {
            if (err) {
                response.status(500);
                response.json({"error":err});
            }
            else {
                response.json({"success":"Slides saved!"});
            }
        });
    }

    /**
     * @brief This function create a new presentation
     * @param response Object (used to return result)
     * @param content JSON string with all presentation info
     * @return JSON string with a message
     */
    static create (content, response){
        content.id = utils.generateUUID();
        content.slides = {};

        fs.writeFile(CONFIG.presentationDirectory+"/"+content.id+".pres.json", JSON.stringify(content), (err) => {
            if (err) {
                response.status(500);
                response.json({"error":err});
            }
            else {
                response.json({"success":"Presentation saved!"});
            }
        });
    }

    /**
     * @brief This function replace all slides of a given presentation
     * @param id The id of a presentation
     * @param callback used to return the results to the controller
     * @return JSON string containing all data
     */
    static read (id, callback){
        let fileName = path.join(CONFIG.presentationDirectory, id + ".pres.json");

        utils.readFileIfExists(fileName, function (err, data) {
            if (err) {
                callback({"error":"Presentation doesn't exist!"});
            }
            else {
                callback(JSON.parse(data));
            }
        });
    }

    /**
     * @brief This function replace all metadata of a given presentation
     * @param response Object (used to return result)
     * @param content JSON string with all presentation info
     * @return JSON string with a message
     */
    update (content, response){
        this.title = content.title;
        this.description = content.description;
        //Trick to be able to access to "this" in all the functions...
        let currentObject = this;

        utils.fileExists(CONFIG.presentationDirectory + "/" + this.id + ".pres.json", function (err) {
            if (err) {
                response.status(400);
                return response.json({"error": "File doesn't exist!"});
            }
            else {
                fs.writeFile(CONFIG.presentationDirectory + "/" + currentObject.id + ".pres.json", JSON.stringify(currentObject), (err) => {
                    if (err) {
                        response.status(500);
                        response.json({"error": err});
                    }
                    else {
                        response.json({"success": "Presentation updated!"});
                    }
                });
            }
        });
    }

    /**
     * @brief This function replace all metadata of a given presentation
     * @param response Object (used to return result)
     * @param id Identifier of the presentation to delete
     * @return JSON string with a message
     */
    static delete (id, response){
        //First check if the file exists
        utils.fileExists(CONFIG.presentationDirectory+"/"+id+".pres.json", function (err, data) {
            if (err){
                response.status(400);
                return response.json({"error":"File doesn't exist!"});
            }
            else {
                //Then if it exists delete it
                fs.unlink(CONFIG.presentationDirectory+"/"+id+".pres.json", (err) => {
                    if (err) {
                        response.status(500);
                        response.json({"error":err});
                    }
                    else {
                        response.json({"success":"Presentation deleted!"});
                    }
                });
            }
        });
    }
}
