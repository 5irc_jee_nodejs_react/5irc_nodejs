//Setup express
var express = require("express");
var router = express.Router();
module.exports = router;

//Import controllers
var PresentationController = require('../controllers/presentation.controller');
var LoginController = require('../controllers/login.controller');



router.get("/presentation", LoginController.CheckAuthentication(false), function(request, response){
    PresentationController.list(response);
});

router.post("/presentation", LoginController.CheckAuthentication(true), function(request, response){
    PresentationController.create(request, response);
});

router.get("/presentation/:presentationId", LoginController.CheckAuthentication(false), function(request, response){
    PresentationController.read(request, response);
});

router.put("/presentation/:presentationId", LoginController.CheckAuthentication(true), function(request, response){
    PresentationController.update(request, response);
});

router.delete("/presentation/:presentationId", LoginController.CheckAuthentication(true), function(request, response){
    PresentationController.delete(request, response);
});

router.post("/presentation/:presentationId/slides", LoginController.CheckAuthentication(true), function(request, response){
    PresentationController.setSlides(request, response);
});

router.get("/presentation/:presentationId/slides", LoginController.CheckAuthentication(false), function(request, response){
    PresentationController.getSlides(request, response);
});



//Manage params
router.param('presentationId', function(req, res, next, presentationId) {
    req.presentationId = presentationId;
    next();
});