var express = require("express");
var router = express.Router();

var LoginController = require('../controllers/login.controller');

module.exports = router;

router.post("/login", LoginController.CheckAuthentication(false), function(request, response){
    response.json({"message":"Authentication succeed"});
});
