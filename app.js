'use strict';

var express = require("express");
var http = require("http");
var CONFIG = require("./config.json");
var app = express();
var path = require("path");

app.use(express.json());

process.env.CONFIG = JSON.stringify(CONFIG);

var server = http.createServer(app);
server.listen(CONFIG.port);


var defaultRoute = require("./app/routes/default.route.js");
app.use(defaultRoute);

var presentationRoute = require("./app/routes/presentation.route.js")
app.use(presentationRoute);

var contentRoute = require("./app/routes/content.route.js")
app.use(contentRoute);

var loginRoute = require("./app/routes/login.route.js")
app.use(loginRoute);

app.use("/admin", express.static(path.join(__dirname, "public/admin")));
app.use("/watch", express.static(path.join(__dirname, "public/watch")));

console.log('STARTED!');