var CONFIG = JSON.parse(process.env.CONFIG);

//Setup multer
var multer = require("multer");
var multerMiddleware = multer({ "dest": CONFIG.uploadDirectory });

//Setup express
var express = require("express");
var router = express.Router();
module.exports = router;

//Import controllers
var ContentController = require('../controllers/content.controller');
var LoginController = require('../controllers/login.controller');



router.get("/content", LoginController.CheckAuthentication(false), function(request, response){
    ContentController.list(response);
});

router.post("/content", LoginController.CheckAuthentication(true), multerMiddleware.single("file"), function(request, response){
    ContentController.create(request, response);
});

router.get("/content/:contentId/:json", LoginController.CheckAuthentication(false), function (request, response) {
    ContentController.read(request, response);
});



//Manage params
router.param('contentId', function(req, res, next, contentId) {
    req.contentId = contentId;

    if (req.params.json === undefined){
        req.metaJson = false;
    }
    else {
        if (req.params.json === "json=true"){
            req.metaJson = true;
        }
        else {
            req.metaJson = false;
        }
    }
    next();
});