let fs = require('fs');
let CONFIG = JSON.parse(process.env.CONFIG);
let path = require("path");

let PresentationModel = require("../models/presentation.model");

module.exports = class ContentController {
    constructor() {
    }

    /**
     * @brief This function list all presentation
     * @param response Object (used to return result)
     * @return JSON string with all presentation
     */
    static list(response) {
        let result = {};

        fs.readdir(CONFIG.presentationDirectory, (err, data) => {
            data.forEach(fileName => {
                if (path.extname(fileName) === ".json") {
                    var content = fs.readFileSync(CONFIG.presentationDirectory + "/" + fileName);
                    var jsonContent = JSON.parse(content);
                    result[jsonContent.id] = jsonContent;
                }
            });
            response.json(result);
        });
    }

    /**
     * @brief This function create a new presentation
     * @param response Object (used to return result)
     * @param request Object with all data to save
     * @return JSON string with error or a message
     */
    static create(request, response){
        let presentationmodel = new PresentationModel(request.body);
        PresentationModel.create(presentationmodel, response);
    }

    /**
     * @brief This read a presentation
     * @param response Object (used to return result)
     * @param request Object with params
     * @return JSON string the presentation
     */
    static read(request, response){
        PresentationModel.read(request.presentationId, (jsonPresentation) => {
            if (jsonPresentation.error !== undefined){
                response.status(400);
            }
            response.json(jsonPresentation);
        });
    }

    /**
     * @brief This function update all metadata of a given presentation
     * @param response Object (used to return result)
     * @param request Object with params
     * @return JSON string with a message
     */
    static update(request, response){
        PresentationModel.read(request.presentationId, (jsonPresentation) => {
            if (jsonPresentation.error !== undefined){
                response.status(400);
                return response.json(jsonPresentation);
            }
            else {
                let presentationmodel = new PresentationModel(jsonPresentation);
                presentationmodel.update(request.body, response);
            }
        });
    }

    /**
     * @brief This function delete a given presentation
     * @param response Object
     * @param request Object with params
     * @return JSON string with a message
     */
    static delete(request, response){
        PresentationModel.delete(request.presentationId, response);
    }

    /**
     * @brief This function replace all slides of a given presentation
     * @param response Object used to return the result
     * @param request Object with params
     * @return JSON string with a message
     */
    static setSlides(request, response){
        PresentationModel.read(request.presentationId, (jsonPresentation) => {
            if (jsonPresentation.error !== undefined){
                response.status(400);
                return response.json(jsonPresentation);
            }
            else {
                let presentationmodel = new PresentationModel(jsonPresentation);
                presentationmodel.setSlides(request.body, response);
            }
        });
    }

    /**
     * @brief This function return all slides of a given presentation
     * @param response Object used to return the result
     * @param request Object with params
     * @return JSON string with all slides
     */
    static getSlides(request, response){
        PresentationModel.read(request.presentationId, (jsonPresentation) => {
            if (jsonPresentation.error !== undefined){
                response.status(400);
                return response.json(jsonPresentation);
            }
            else{
                response.json(jsonPresentation.slides);
            }
        });
    }
}